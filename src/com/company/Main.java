package com.company;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.IOException;

public class Main {

    private static final String ID = "make-everything-ok-button";
    private static final String[] attributesPriority = {"href", "title", "rel", "onclick"};

    private static XPath xpath;
    private static DocumentBuilder builder;

    public static void main(String[] args) throws Exception {
        String originFilePath = args[0];
        String sampleFilePath = args[1];
        String tagId = args.length > 2 && !args[2].isEmpty() ? args[2] : ID;

        initializeXpathAndBuilder();
        Node node = null;
        try {
            node = (Node) xpath.evaluate("//*[@id='" + tagId + "']", builder.parse(originFilePath), XPathConstants.NODE);
        } catch (IOException e) {
            System.out.println("Couldn't find or read origin file");
            System.exit(0);
        }

        if (node == null) {
            System.out.println("Couldn't find node with this id in the origin file");
            return;
        }

        String originalTagText = node.getTextContent().trim();
        NamedNodeMap originalTagAttributes = node.getAttributes();

        String pathToNode = searchNode(originalTagText, sampleFilePath, originalTagAttributes);

        System.out.println(pathToNode);
    }

    private static void initializeXpathAndBuilder() throws Exception {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        xpath = xPathfactory.newXPath();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
    }

    private static String searchNode(String originalTagText, String sampleFilePath, NamedNodeMap originalTagAttributes) throws Exception {

        Node node = null;
        try {
            node = (Node) xpath.evaluate("//*[normalize-space(text())  = '" + originalTagText + "']", builder.parse(sampleFilePath), XPathConstants.NODE);
        } catch (IOException e) {
            System.out.println("Couldn't find or read sample file");
            System.exit(0);
        }

        if (node != null) {
            return getPathToNode(node);
        } else {
            NodeList nodeList;
            String expression;
            for (String attribute : attributesPriority) {
                Node item = originalTagAttributes.getNamedItem(attribute);
                if (item != null) {
                    expression = "//*[@" + item.getNodeName() + "='" + item.getNodeValue() + "']";
                    nodeList = (NodeList) xpath.evaluate(expression, builder.parse(sampleFilePath), XPathConstants.NODESET);

                    if (nodeList.getLength() == 1) {
                        node = nodeList.item(0);
                        return getPathToNode(node);
                    }
                }
            }
        }

        return "Couldn't find the node in sample file";
    }


    private static String getPathToNode(Node node) {
        Node parent = node.getParentNode();
        if (parent == null) {
            return node.getNodeName();
        }

        NodeList children = parent.getChildNodes();

        int counter = 0, finalCounter = 1;
        Node childrenNode;
        for (int i = 0; i < children.getLength(); i++) {
            childrenNode = children.item(i);
            if (childrenNode.getNodeName().equals(node.getNodeName())) {
                counter++;
                if (childrenNode.isEqualNode(node)) finalCounter = counter;

            }
        }

        String result = getPathToNode(parent) + "/" + node.getNodeName();
        if (counter > 1) {
            result += "[" + finalCounter + "]";
        }

        return result;
    }

}
